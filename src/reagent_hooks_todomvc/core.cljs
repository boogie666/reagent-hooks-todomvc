(ns reagent-hooks-todomvc.core
  (:require [reagent.core :as r :refer [atom]]
            [reagent-hooks.core :as h]
            [reagent-hooks-todomvc.redux :refer [dispatch! app-state]]
            [reagent-hooks-todomvc.hooks :as hooks]
            [reagent.core :as r]
            [clojure.string :as str]
            [reagent-hooks-todomvc.controller :as controller]))

(enable-console-print!)

(defn enter-pressed? [key-code]
  (= 13 key-code))

(defn todo-input []
  (let [[title set-title!] (hooks/use-text-input "")
        input (h/use-ref)
        _ (hooks/use-autofocus input)]
    (fn []
      [:input.new-todo {:type "text"
                        :value @title
                        :ref input
                        :placeholder "What needs to be done?"
                        :on-change set-title!
                        :on-key-down #(when (and (enter-pressed? (.-keyCode %)) (not= "" (str/trim @title)))
                                        (dispatch! [:todos/new-todo {:title @title}])
                                        (set-title! nil))}])))

(defn todo-checkbox [{:keys [id done]}]
  [:input.toggle {:type "checkbox"
                  :checked done
                  :on-change #(dispatch! [:todos/toggle {:id id}])}])

(defn todo-view [done! todo]
  [:div.view
   [todo-checkbox todo]
   [:label {:on-double-click done!} (:title todo)]
   [:button.destroy {:on-click #(dispatch! [:todos/delete {:id (:id todo)}])}]])

(defn todo-edit [done! {:keys [id title]}]
  (let [input (h/use-ref)
        [new-title set-new-title!] (hooks/use-text-input title)
        _ (hooks/use-autofocus input)
        _ (h/use-will-unmount #(when-not (= title @new-title)
                                 (dispatch! [:todos/save-title {:id id :title @new-title}])))]
    (fn [_ _]
      [:input.edit {:type "text"
                    :ref input
                    :value @new-title
                    :on-change set-new-title!
                    :on-blur done!
                    :on-key-down #(when (enter-pressed? (.-keyCode %))
                                    (done!))}])))

(defn todo-item [{:keys [title]}]
  (let [[editing? set-editing!] (h/use-state false)]
    (fn [{:keys [id title done] :as todo}]
      [:li {:class (str (when done "completed") (when @editing? " editing"))}
       (if-not @editing?
         [todo-view (partial set-editing! true) todo]
         [todo-edit (partial set-editing! false) todo])])))

(defn todo-list [todos]
  [:ul.todo-list
   (for [todo todos]
     ^{:key (:id todo)}
     [todo-item todo])])

(defn todo-filters [visible]
  [:ul.filters
   [:li [:a {:class (when (= :all visible) "selected")
             :style {:cursor "pointer"}
             :on-click #(dispatch! [:todos/show :all])}  "All"]]
   [:li [:a {:class (when (= :active visible) "selected")
             :style {:cursor "pointer"}
             :on-click #(dispatch! [:todos/show :active])} "Active"]]
   [:li [:a {:class (when (= :completed visible) "selected")
             :style {:cursor "pointer"}
             :on-click #(dispatch! [:todos/show :completed])} "Completed"]]])



(defn todo-toggle-all [todos]
  (when (seq todos)
    [:span
      [:input#toggle-all.toggle-all {:type "checkbox"
                                      :checked (every? :done todos)
                                      :on-change #(dispatch! [:todos/toggle-all])}]
      [:label {:for "toggle-all"} "Mark all as complete"]]))


(defn todo-count [todos]
  (let [done-count (count (remove :done todos))
        items-word (if-not (= 1 done-count) "items" "item")]
    [:span.todo-count
      (str done-count " " items-word " left")]))

(defn todo-app []
  (hooks/use-autosave app-state :todos)
  (fn []
    [:div
     [:section.todoapp
      [:header.header
       [:h1 "todos"]
       [todo-input]]
      [:section.main
       [todo-toggle-all (controller/todos @app-state)]
       [todo-list (controller/visible-todos @app-state)]]
      [:footer.footer
       [todo-count (controller/todos @app-state)]
       [todo-filters (:visible @app-state)]]]]))

(r/render [todo-app]
          (. js/document (getElementById "app")))

(defn on-js-reload [])

