(ns reagent-hooks-todomvc.hooks
  (:require [reagent-hooks.core :as h]
            [clojure.reader :as reader]))

(defn use-autosave [atom key]
  (h/use-watch atom (fn [new-value]
                      (.setItem js/localStorage (pr-str key) (pr-str (key new-value)))))
  (h/use-did-mount (fn []
                     (let [from-storage (reader/read-string (.getItem js/localStorage (pr-str key)))]
                       (swap! atom assoc key from-storage)))))

(defn use-text-input [initial]
  (let [[state set-state!] (h/use-state initial)]
    [state #(set-state! (if-not % "" (-> % .-target .-value)))]))

(defn use-autofocus [input]
  (h/use-did-mount (fn []
                     (let [value (-> @input .-value)
                           length (count value)]
                       (.setSelectionRange @input length length)
                       (.focus @input)))))
