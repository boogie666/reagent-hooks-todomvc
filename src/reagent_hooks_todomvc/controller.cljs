(ns reagent-hooks-todomvc.controller)


(defn new-todo [title]
  {:id (random-uuid)
   :title title
   :timestamp (.getTime (js/Date.))
   :done false})

(defn create-new-todo [state title]
  (let [todo (new-todo title)]
    (assoc-in state [:todos (:id todo)] todo)))

(defn toggle-todo-state [state id]
  (update-in state [:todos id :done] not))

(defn save-todo [state id new-title]
  (assoc-in state [:todos id :title] new-title))

(defn delete-todo [state id]
  (update-in state [:todos] dissoc id))

(defn show-todos [state visible]
  (assoc state :visible visible))

(defn- map-values [f]
  (map (fn [[key val]] [key (f val)])))

(defn toggle-all [state]
  (let [done? (every? :done (-> state :todos vals))]
    (update state :todos #(into {} (map-values (fn [value] (assoc value :done (not done?)))) %))))

(defn todos [state]
  (->> state :todos vals))

(defn visible-todos [state]
  (let [s (:visible state)
        todos (->> state todos (sort-by :timestamp))]
    (case s
      :all todos
      :active (filter (comp not :done) todos)
      :completed (filter :done todos))))



