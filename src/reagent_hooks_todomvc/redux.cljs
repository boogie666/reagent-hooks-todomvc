(ns reagent-hooks-todomvc.redux
  (:require [reagent-hooks-todomvc.controller :as controller]
            [clojure.spec.alpha :as s]
            [reagent.core :as r]))

(defonce app-state (r/atom {:todos {}
                            :visible :all}))

(when goog/DEBUG
  (s/check-asserts true))

(s/def :todo/id uuid?)
(s/def :todo/title string?)
(s/def :todo/timestamp nat-int?)
(s/def :todo/done boolean?)

(s/def :todos/todo
  (s/keys :req-un [:todo/id :todo/title :todo/timestamp :todo/done]))

(s/def :state/todos
  (s/map-of uuid? :todos/todo))

(s/def :state/visible #{:all :active :completed})

(s/def :state/db
  (s/keys :req-un [:state/todos :state/visible]))


(defn action-spec [id payload]
  (s/cat :id #{id}
         :payload payload))

(s/def :actions/new-todo
  (action-spec
   :todos/new-todo
   (s/keys :req-un [:todo/title])))

(s/def :actions/toggle
  (action-spec
   :todos/toggle
   (s/keys :req-un [:todo/id])))

(s/def :actions/save-title
  (action-spec
   :todos/save-title
   (s/keys :req-un [:todo/id :todo/title])))


(s/def :actions/delete
  (action-spec
   :todos/delete
   (s/keys :req-un [:todo/id])))

(s/def :actions/show
  (action-spec
   :todos/show
   :state/visible))

(s/def :actions/toggle-all
  (s/cat :id #{:todos/toggle-all}))

(s/def :redux/action
  (s/or :new-todo :actions/new-todo
        :toggle :actions/toggle
        :save-title :actions/save-title
        :delete :actions/delete
        :show :actions/show
        :toggle-all :actions/toggle-all))

(defn reducer [state [action payload]]
  (case action
    :todos/new-todo (controller/create-new-todo state (:title payload))
    :todos/toggle (controller/toggle-todo-state state (:id payload))
    :todos/save-title (controller/save-todo state (:id payload) (:title payload))
    :todos/delete (controller/delete-todo state (:id payload))
    :todos/show (controller/show-todos state payload)
    :todos/toggle-all (controller/toggle-all state)
    state))


(defn dispatch! [action]
  {:pre [(s/assert :redux/action action)]
   :post [(s/assert :state/db %)]}
  (swap! app-state reducer action))



(s/fdef dispatch!
        :args :redux/action
        :ret :state/db)

